FROM python:latest
LABEL maintainer="gianluca.rigoletti@cern.ch"

RUN mkdir /app

RUN apt-get update && \
    apt-get install -y gcc g++ wget unzip rsync nano -y && \
    wget https://nexus.web.cern.ch/nexus/service/local/repositories/cern-nexus/content/cern/dip/dip/5.7.0/dip-5.7.0-distribution.zip -O /tmp/dip.zip && \
    unzip /tmp/dip.zip && \
    mv dip-5.7.0 /dip && \
    rm -rf /var/lib/apt/lists/*

COPY requirements.txt /requirements.txt
RUN pip install -r /requirements.txt

ENV DIPBASE=/dip
ENV LD_LIBRARY_PATH=$DIPBASE/lib64:$LD_LIBRARY_PATH

RUN chgrp -R 0 /app && \
    chmod -R g=u /app
COPY app/ /app/

USER 10001
CMD [ "python", "/app/main.py" ]