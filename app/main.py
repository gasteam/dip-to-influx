import cppyy
import os
from pathlib import Path
import time
import ctypes
import os
import datetime
from pydantic import BaseSettings, Field
from pydantic.types import FilePath
from typing import Dict, List
from influxdb import InfluxDBClient
import logging
import urllib3
from threading import RLock
import json

urllib3.disable_warnings()

lock = RLock()

class InfluxDBSettings(BaseSettings):
    host: str = Field(..., env='INFLUXDB_HOST')
    port: int = Field(..., env='INFLUXDB_PORT')
    username: str = Field(..., env='INFLUXDB_USERNAME')
    password: str = Field(..., env='INFLUXDB_PASSWORD')
    database: str = Field(..., env='INFLUXDB_DATABASE')
    ssl: bool = Field(True, env='INFLUXDB_SSL')
    verify_ssl: bool = Field(False, env='INFLUXDB_VERIFYSSL')
    measurement: str = Field("dip", env='MEASUREMENT')
    tags: Dict[str, str] = {}


class GlobalSettings(BaseSettings):
    influxdb_settings: InfluxDBSettings = InfluxDBSettings()
    subscriptions_filepath: FilePath = Field("subscriptions.txt",
                                             env='SUBSCRIPTIONS_FILEPATH')
    dip_nameservers: str = Field("dipnsgpn1.cern.ch,dipnsgpn2.cern.ch", env='DIP_NAMESERVERS')
    logging_level: str = Field('INFO', env='LOGGING_LEVEL')
    client_name: str = Field(os.getenv('HOSTNAME', 'dip-client'), env='CLIENT_NAME')
    poll_time: int = Field(5, env='POLL_TIME')

    class Config:
        env_file = '.env'


# Import dip library
dip_dir = Path('/dip')
cppyy.add_library_path(str(dip_dir / 'lib64/'))
cppyy.add_include_path(str(dip_dir / 'include/'))
cppyy.include(str(dip_dir / 'include' / 'Dip.h'))
cppyy.include(str(dip_dir / 'include' / 'DipSubscription.h'))
cppyy.include(str(dip_dir / 'include' / 'DipData.h'))
cppyy.include(str(dip_dir / 'include' / 'DipDataType.h'))
cppyy.load_library(str(dip_dir / 'lib64' / 'libdip.so'))
cppyy.load_library(str(dip_dir / 'lib64' / 'liblog4cplus.so'))
log4cplus_properties_path = dip_dir / 'log4cplus.properties'
c = cppyy.gbl

# Define custom function for retrieving tags. Somehow the internal cppyy
# conversion doesn't work properly with const char ** (it sees it as a int*)
cppyy.cppdef("""
    std::vector<std::string> GetTags(DipData &message) {
        int ntags;
        const char **tags = message.getTags(ntags);
        std::vector<std::string> tagsVector;
        for (int i = 0; i < ntags; i++) {
            const char* tag = tags[i];
            tagsVector.push_back(tag);
        }
        return tagsVector;
    }

    const char * stringToChar(std::string in) {
        return in.c_str();
    }
""")

dip_types = {
    c.TYPE_NULL: "TYPE_NULL",
    c.TYPE_BOOLEAN: "TYPE_BOOLEAN",
    c.TYPE_BOOLEAN_ARRAY: "TYPE_BOOLEAN_ARRAY",
    c.TYPE_BYTE: "TYPE_BYTE",
    c.TYPE_BYTE_ARRAY: "TYPE_BYTE_ARRAY",
    c.TYPE_SHORT: "TYPE_SHORT",
    c.TYPE_SHORT_ARRAY: "TYPE_SHORT_ARRAY",
    c.TYPE_INT: "TYPE_INT",
    c.TYPE_INT_ARRAY: "TYPE_INT_ARRAY",
    c.TYPE_LONG: "TYPE_LONG",
    c.TYPE_LONG_ARRAY: "TYPE_LONG_ARRAY",
    c.TYPE_FLOAT: "TYPE_FLOAT",
    c.TYPE_FLOAT_ARRAY: "TYPE_FLOAT_ARRAY",
    c.TYPE_DOUBLE: "TYPE_DOUBLE",
    c.TYPE_DOUBLE_ARRAY: "TYPE_DOUBLE_ARRAY",
    c.TYPE_STRING: "TYPE_STRING",
    c.TYPE_STRING_ARRAY: "TYPE_STRING_ARRAY"   ,
}

influxdb_map_types = {
    c.TYPE_NULL: "Bool",
    c.TYPE_BOOLEAN: "Int",
    c.TYPE_BYTE: "Int",
    c.TYPE_SHORT: "Int",
    c.TYPE_INT: "Int",
    c.TYPE_LONG: "Int",
    c.TYPE_FLOAT: "Float",
    c.TYPE_DOUBLE: "Float",
    c.TYPE_STRING: "String",
}

global_store = []

def parse_subscriptions(filepath):
    subs = []
    with open(filepath, 'r') as f:
        for line in f:
            s = line.strip()
            if s != '':
                subs.append(s)
    return subs


class GeneralDataListener(c.DipSubscriptionListener):
    """Subclass DIP's class to handle each received message. 
    
    Each time a message is recevied it goes through handleMessage where it actually
    conver it to a proper type for influxdb.

    handleException is not clear when it's called but needs to be subclassed

    connected() is called upon connection and disconnected() as well
    """
    def __init__(self, settings: GlobalSettings) -> None:
        super().__init__()
        self.settings = settings
        self.logger = logging.getLogger(__name__)

    def handleMessage(self, subscription, message):
        try:
            tags = cppyy.gbl.GetTags(message)
            for tag in tags:
                tag = tag.c_str()
                value_type = message.getValueType(tag)
                influx_db_value_map = influxdb_map_types[value_type]
                topic = subscription.getTopicName()
                if value_type == c.TYPE_FLOAT:
                    value = message.extractFloat(tag)
                elif value_type == c.TYPE_DOUBLE:
                    value = message.extractDouble(tag)
                elif value_type == c.TYPE_BOOLEAN:
                    value = message.extractBool(tag)
                    value = int(value)
                elif value_type == c.TYPE_INT:
                    value = message.extractInt(tag)
                elif value_type == c.TYPE_LONG:
                    value = message.extractLong(tag)
                elif value_type == c.TYPE_BYTE:
                    value = message.extractByte(tag)
                elif value_type == c.TYPE_STRING:
                    continue  # skip string handling
                    value = message.extractString(tag)
                else:
                    logging.warning(f"Type {dip_types[value_type]} {value_type} not supported yet")
                    continue

                dip_timestamp = message.extractDipTime()
                nano_timestamp = dip_timestamp.getAsNanos()

                with lock:
                    updated = False
                    global global_store
                    for item in global_store:
                        if item['topic'] == topic and item['tag'] == f"tag{tag}":
                            self.logger.debug(f"Updating {item}")
                            item.update({
                                'timestamp': nano_timestamp,
                                'influxdb_value': influx_db_value_map,
                                'value': value
                            })
                            updated = True
                    if not updated:
                        global_store.append({
                            'topic': topic,
                            'tag': f"tag{tag}",
                            'timestamp': nano_timestamp,
                            'influxdb_value': influx_db_value_map,
                            'value': value
                        })
                        self.logger.debug(f"Item update to {item}")
                    self.logger.debug(f'Global store: {global_store}')


        except Exception as e:
            self.logger.exception(e, flush=True)

    def handleException(self, subscription, message):
        print('Handle exception: ', subscription, message.toString(), flush=True)

    def connected(self, subscription):
        print('Connected: ', type(subscription), flush=True)
        subscription.requestUpdate()

    def disconnected(self, subscription, reason):
        print('Disconnected: ', type(subscription), reason, flush=True)

if __name__ == '__main__':
    logger = logging.getLogger(__name__)
    # Parse settings
    settings = GlobalSettings()
    logging.basicConfig(level=settings.logging_level)
    influxdb_settings = settings.influxdb_settings
    influxdb_client = InfluxDBClient(
        influxdb_settings.host,
        influxdb_settings.port,
        influxdb_settings.username,
        influxdb_settings.password,
        influxdb_settings.database,
        ssl=influxdb_settings.ssl,
        verify_ssl=influxdb_settings.verify_ssl)
    subscriptions_names = parse_subscriptions(settings.subscriptions_filepath)
    logger.info(f"Subscriptions names: {subscriptions_names}")
    # Create handler for multiple subscriptions
    handler = GeneralDataListener(settings)
    # Create a client and give it a name on the network
    dip = c.Dip.create(settings.client_name)
    # Set DNS Name server
    dip.setDNSNode(settings.dip_nameservers)
    subscriptions = dict() # Map from subscription name to DipSubscription
    for subscription in subscriptions_names:
        logger.debug(f'Subscription: {subscription}')
        sub = dip.createDipSubscription(subscription, handler)
        subscriptions[subscription] = sub
        time.sleep(0.5)
    def update_subscriptions():
        new_subscriptions_set = set(parse_subscriptions(settings.subscriptions_filepath))
        old_subscriptions_set = set(subscriptions.keys())
        add_subs = new_subscriptions_set - old_subscriptions_set
        destroy_subs = old_subscriptions_set - new_subscriptions_set
        for subscription in destroy_subs:
            logger.debug(f'Destroy subscription: {subscription}')
            sub = dip.destroyDipSubscription(subscriptions[subscription])
            subscriptions.pop(subscription)
            time.sleep(0.5)
            for item in global_store:
                if item['topic'] == subscription:
                    global_store.remove(item)
                    break
        for subscription in add_subs:
            logger.debug(f'Add subscription: {subscription}')
            sub = dip.createDipSubscription(subscription, handler)
            subscriptions[subscription] = sub
            time.sleep(0.5)
    while True:
        time.sleep(settings.poll_time)
        update_subscriptions()
        # Insert data to DB
        payload = [{
            "measurement": influxdb_settings.measurement,
            "tags": {
                "sub": item['topic'],
                "dip_tag": item['tag'],
                **influxdb_settings.tags
            },
            "time": time.time_ns(),
            "fields": {
                f"{item['influxdb_value']}_value": item["value"]
            }
        } for item in global_store]
        try:
            logger.debug(json.dumps(payload, indent=2))
            res = influxdb_client.write_points(payload, protocol='json', time_precision='n')
            logger.debug(f"Result of writing points: {res}")
        except Exception as e:
            logging.debug("Payload: ")
            logging.debug(payload)
            logging.exception(e)